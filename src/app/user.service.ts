import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/firestore";
import {Observable} from "rxjs";
import {UserModel} from "./__model/users.model";
import {AuthenticationService} from "./authentication.service";
import {snapshotChanges} from "@angular/fire/database";
@Injectable({
  providedIn: 'root'
})
export class UserService {

  //docRef=this.firestore.collection("wished").doc(this.auth.getId());
  obj: any;
  array:any[]=[];
  counter1: number;
  //user: UserModel={name:"",surname:"",email:"",password:"",role:"",id:"",counter:0}

  blah: any;

  constructor(private firestore: AngularFirestore,private auth: AuthenticationService) { }

  getAllUsers(): Observable<UserModel[]> {

    return this.firestore.collection('users').valueChanges() as Observable<UserModel[]>;

  }

  /*setCounterFromUserDb(){
    this.firestore.collection('users').doc(this.auth.getDoc()).get().toPromise().then((doc) => {
      if (doc.exists) {
        this.obj=doc.data();
        this.array=Object.values(this.obj);
        this.setCounterUser(this.array[0]);
        console.log(this.obj);
      } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
      }
    }).catch((error) => {
      console.log("Error getting document:", error);
    });


  }
  setCounterUser(c:number){
    console.log(c);
    this.counter1=c;
  }

  getCounterUser():number{
    console.log(this.counter1);
    return this.counter1;
}*/


}
