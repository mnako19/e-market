import { Injectable, NgZone } from '@angular/core';
import {User } from "./__model/user"
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  loggedIn = false;
  role: string="";
  userId: string='';
  counter: number;
  doc: string;

  setDoc(name, surname: string){
    this.doc=name+surname;
  }
  getDoc():string{
    return this.doc;
  }

  setCounter(nr: number){
  this.counter=nr;

}
  getCounter():number{
  return this.counter;
}


  isLoggedIn():boolean {
    return  this.loggedIn;
  }

  setLoggedIn(loggedIn: boolean): void {
    this.loggedIn = loggedIn;
  }

  setRole(role: string){

    this.role=role;
  }

  getRole(): string{
    return this.role;
  }

  setId(id: string): void {
    this.userId=id;
  }

  getId(): string{
    return this.userId;
  }


}
