import { Component, OnInit, Input, Output } from '@angular/core';
import {ImageService} from "../shared/image.service";
import {productModel} from "../__model/productModel";
import {ProdService} from "../shared/prod.service";
import { filter } from "rxjs/operators";
import { interval, of, timer } from "rxjs";
import { fromArray } from "rxjs/internal/observable/fromArray";
import {AuthenticationService} from "../authentication.service";
import {AngularFireDatabase, AngularFireList} from "@angular/fire/database";
import {Router} from "@angular/router";
import {query} from "@angular/animations";
import {AngularFirestore} from "@angular/fire/firestore";
import {wishedProductModel} from "../__model/wishedProductModel";
import {UserModel} from "../__model/users.model";
import {UserService} from "../user.service";

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {


  productList: productModel[];
  title: string;
 // c:number=this.auth.getCounter()+1;
  in: number;//=this.auth.getCounter()+1;

  users:any;
  allUsers: UserModel[] = [];
  category: string;
  rowIndexArray: any[];
  constructor(private userService: UserService, private firestore: AngularFirestore, private prodService: ProdService, router: Router,private auth: AuthenticationService, private imageService: ImageService, private firebase:AngularFireDatabase) {
  }

  ngOnInit(): void {
    this.userService.getAllUsers().subscribe((response: UserModel[]) => {
      this.allUsers = response;
    })
    this.getC();

    this.in=this.auth.getCounter();

    this.prodService.notifyObservable$.subscribe(res => {

      if(res.refresh){
        this.title=this.prodService.getCategory();
        this.imageService.getFilteredList('category',this.prodService.getCategory());

        this.imageService.imageDetailList.snapshotChanges().subscribe(
          list=>{
            this.productList= list.map(item=> {return item.payload.val();});
            this.rowIndexArray=Array.from(Array(Math.ceil(this.productList.length/4)).keys())
          }
        );
      }
    })




  }
  getC(){
    this.allUsers.forEach((user: UserModel) => {

      if (user.id=this.auth.getId()) {
        this.auth.setCounter(user.counter);
      }
    })
  }


    getProdId(prodId: string) {

      this.firestore.collection('users').doc(this.auth.getDoc()).set({counter:this.in},{ merge: true })
      console.log(this.in);
      this.firestore.collection('wished').doc(this.auth.getId()).set({['prod'+this.in]: prodId},{ merge: true })
      this.in+=1;
    }

  isLog(): boolean{
    return this.auth.isLoggedIn();
  }




}
