import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {USER} from "../comp/signup/signup.component";
import {AngularFirestore} from "@angular/fire/firestore";
import {AuthenticationService} from "../authentication.service";
import {ProdService} from "../shared/prod.service";

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  formTemplate: FormGroup;
 // user:USER;
  submitted:boolean=false;
  trueSubmitted: boolean=false;
  total: number;
  constructor(private prodService: ProdService, private db: AngularFirestore, private auth: AuthenticationService) { }

  ngOnInit(): void {
    this.formTemplate=new FormGroup({
      name: new FormControl('',Validators.required),
      cardNo: new FormControl('', [Validators.required,Validators.minLength(16), Validators.maxLength(16)]),
      exDate: new FormControl('',Validators.required),
      cvv: new FormControl('', Validators.required)
    });

    this.total=this.prodService.getTotal();
    console.log(this.total);
  }

  get f() { return this.formTemplate.controls; }

  submit(){
    this.submitted=true;

    if (this.formTemplate.invalid) {
      return;
    }


    this.db.collection('wished').doc(this.auth.getId()).delete();
    this.db.collection('users').doc(this.auth.getDoc()).set({counter:0},{ merge: true });


    this.trueSubmitted=true;
  }



}
