import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import {UserModel} from "../../__model/users.model";
import {AngularFirestore} from "@angular/fire/firestore";
import {AuthenticationService} from "../../authentication.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  formTemplate: FormGroup;
  user:USER;
  submitted:boolean=false;
  trueSubmitted:boolean=false;
//initialize the user class

  constructor(private firestore: AngularFirestore,private auth: AuthenticationService) { }

  ngOnInit(): void {
  this.formTemplate=new FormGroup({
  name: new FormControl('',Validators.required),
  surname: new FormControl('',Validators.required),
  email: new FormControl('',Validators.email),
  password: new FormControl('', [Validators.required,Validators.minLength(8)]),
  passwordConfirm: new FormControl('',[Validators.required])
},{ validators: this.checkPasswords });


    this.user=new USER(this.formTemplate.get('name').value,
      this.formTemplate.get('surname').value,
      this.formTemplate.get('email').value,
      this.formTemplate.get('password').value,
      this.firestore,this.auth);
  }
  get f() { return this.formTemplate.controls; }
  submit(){
    this.submitted=true;
    if (this.formTemplate.invalid) {
      return;
    }

    this.user.setValues(this.formTemplate.get('name').value,
      this.formTemplate.get('surname').value,
      this.formTemplate.get('email').value,
      this.formTemplate.get('password').value);



    this.user.setInDb();
    this.trueSubmitted=true;

  }
  checkPasswords: ValidatorFn = (group: AbstractControl):  ValidationErrors | null => {
    let pass = group.get('password').value;
    let confirmPass = group.get('passwordConfirm').value
    return pass === confirmPass ? null : { notSame: true }
  }


}
export class USER{
  name: string;
  surname: string;
  email: string;
  password: string;
  role: string='user';
  id: string='0005';
  counter:number=0;

  constructor(name: string, surname:string, email:string, password:string,private firestore: AngularFirestore,private auth: AuthenticationService) {
    this.name=name;
    this.surname=surname;
    this.email=email;
    this.password=password;
  }
  setValues(name: string, surname:string, email:string, password:string){
    this.name=name;
    this.surname=surname;
    this.email=email;
    this.password=password;
  }

  setInDb(){
    this.firestore.collection('users').doc(this.name+this.surname).set({name:this.name},{ merge: true })
    this.firestore.collection('users').doc(this.name+this.surname).set({surname:this.surname},{ merge: true })
    this.firestore.collection('users').doc(this.name+this.surname).set({email:this.email},{ merge: true })
    this.firestore.collection('users').doc(this.name+this.surname).set({password:this.password},{ merge: true })
    this.firestore.collection('users').doc(this.name+this.surname).set({id:this.id},{ merge: true })
    if(this.auth.getRole()==='admin') {
      this.firestore.collection('users').doc(this.name+this.surname).set({role:'admin'},{ merge: true })}
      else{
    this.firestore.collection('users').doc(this.name+this.surname).set({role:'user'},{ merge: true })}
    this.firestore.collection('users').doc(this.name+this.surname).set({counter:0},{ merge: true })


  }

}
