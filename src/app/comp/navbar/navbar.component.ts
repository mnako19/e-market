import {Component, Input, OnInit} from '@angular/core';
import {AuthenticationService} from "../../authentication.service";
import {LoginComponent} from "../login/login.component";
import {ActivatedRoute} from "@angular/router";
import {ProdService} from "../../shared/prod.service";
import {ImageService} from "../../shared/image.service";
import {productModel} from "../../__model/productModel";
import {from, Observable} from 'rxjs';
import { filter } from 'rxjs/operators';
import {fromArray} from "rxjs/internal/observable/fromArray";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  logged: boolean;
  roleType: string;
  currentItem:any;
  prodList: productModel[];
  filterList: productModel[];
  rowIndexArray: any[];
  aString: string;
  //value to be exported to the child component

  category: string='Makeup';
  constructor(private auth: AuthenticationService, private imageService: ImageService, private prodService: ProdService) {
  }

  ngOnInit(): void {


  }

  refreshGridInAnotherComponent(){
    this.prodService.notifyOther({refresh: true});
  }
  getCategoryNav(category: string){
    //gets the name of category when search bar is clicked
    //sets the value to the data filed
    this.prodService.notifyOther({refresh: true});
    this.category=category;
    this.prodService.setCategory(category);
  }

  setAString(category: string){
    this.aString=category;
  }

  isLog(): boolean{
    return this.auth.isLoggedIn();
  }
  roleF(): string{
    return this.auth.getRole();
  }
}






