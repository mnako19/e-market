import { Component, OnInit } from '@angular/core';
import {ROUTES} from "@angular/router";
import {RouterModule} from "@angular/router";
import {AuthenticationService} from "../../authentication.service";
import {UserModel} from "../../__model/users.model";
import {Router} from "@angular/router";
import {UserService} from "../../user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  users:any;
  allUsers: UserModel[] = [];
  constructor(private authService: AuthenticationService,
              private router: Router,
              private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getAllUsers().subscribe((response: UserModel[]) => {
      this.allUsers = response;
    })
  }

  login(email: string, password: string) {
    this.allUsers.forEach((user: UserModel) => {

      if (user.email === email && user.password === password) {
        this.authService.setLoggedIn(true);
        this.authService.setId(user.id);
        this.authService.setCounter(user.counter);
        this.authService.setDoc(user.name,user.surname);
        if(user.role==='admin')
        {

          this.router.navigate(['/images/upload']);
          this.authService.setRole('admin');
        }
        else {

          this.router.navigate(['/images/list']);
          this.authService.setRole('user');
        }
      } else {
        console.error("User does not exist")
      }
    })
  }


}
