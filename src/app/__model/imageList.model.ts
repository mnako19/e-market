export interface imageModel{
  userId: string;
  caption: string;
  category: string;
  price:string;
  quantity: string;
  imageURL: string;
  imageUrl: string;
}
