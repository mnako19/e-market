export interface UserModel {
  name: string;
  surname: string;
  email: string;
  password: string;
  role: string;
  id: string;
  counter: number;
}
