import { Injectable } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from "@angular/fire/database";
import {BehaviorSubject, Observable} from "rxjs";
import {productModel} from "../__model/productModel";
import {AngularFirestore} from "@angular/fire/firestore";
import {UserModel} from "../__model/users.model";
import {wishedProductModel} from "../__model/wishedProductModel";
import {AuthenticationService} from "../authentication.service";

@Injectable({
  providedIn: 'root'
})
export class ProdService {
  public notify = new BehaviorSubject<any>('');

  notifyObservable$ = this.notify.asObservable();
  total:number;
  wishProduct: any;
  category: string;
  constructor(private firebase:AngularFireDatabase,private firestore: AngularFirestore, private auth: AuthenticationService) { }


  public notifyOther(data: any) {
    if (data) {
      this.notify.next(data);
    }
  }


  /*getAllWished(): Observable<wishedProductModel> {
    return this.firestore.collection('wished').doc(this.auth.getId()).valueChanges() as Observable<wishedProductModel>;
  }

  getDoc(){
 this.firestore.collection('wished').doc(this.auth.getId()).get();
  }*/

  setTotal(tot: number){
    this.total=tot;
  }
  getTotal(){
    return this.total;
  }
  setCategory(category: string){
    this.category=category;

  }
  getCategory(): string{
    return this.category
  }

  setWishProd(prod: any){
    this.wishProduct=prod;
  }


  getWishProd(): any{
    return this.wishProduct;
  }

}
