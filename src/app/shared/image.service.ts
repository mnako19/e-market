import { Injectable } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from "@angular/fire/database";
import firebase from "firebase";
import {Observable} from "rxjs";
import { map } from 'rxjs/operators';
import {productModel} from "../__model/productModel";

@Injectable({
  providedIn: 'root'
})
export class ImageService {

imageDetailList: AngularFireList<any>;

items: Observable<any>;

  constructor(private firebase:AngularFireDatabase) {

  }

  getImageDetailList(){
    this.imageDetailList=this.firebase.list('imageDetails');
  }

  getFilteredList(child,equalsTo: string){
    this.imageDetailList= this.firebase.list('imageDetails', ref=> ref.orderByChild(child).equalTo(equalsTo));
  }


  getItemsObservable(){
    return this.items;
}

  insertImageDetails(imageDetails){
    this.imageDetailList.push(imageDetails);
  }


}
