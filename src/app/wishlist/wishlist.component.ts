import { Component, OnInit } from '@angular/core';
import {imageModel} from "../__model/imageList.model";
import {FormGroup, FormControl} from "@angular/forms";
import {finalize} from "rxjs/operators";
import {ProdService} from "../shared/prod.service";
import {ImageService} from "../shared/image.service";
import {wishedProductModel} from "../__model/wishedProductModel";
import {productModel} from "../__model/productModel";
import {AngularFirestore} from "@angular/fire/firestore";
import {AuthenticationService} from "../authentication.service";
import * as admin from "firebase-admin";
import firebase from "firebase";

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit {

  wishListList:wishedProductModel;
  array:string[];
  arrFields: string[];
  arrVal: string[];
  wishedProductList: productModel[];
  filled:boolean=false;
  empty:boolean[];
  copyList:productModel[]=[ {name:"",id:"",caption:"",category:"",imageURL:"",imageUr:"",price:"",quantity:""}];

  obj:any;
  total:number=0;
  constructor(private auth: AuthenticationService,private db: AngularFirestore,private prodService: ProdService, private imageService : ImageService) { }
  docRef = this.db.collection("wished").doc(this.auth.getId());
  //admin = require("firebase-admin");
  ngOnInit(): void {

    this.getWishProd();

  }

  getWishProd(){
    this.docRef.get().toPromise().then((doc) => {
      if (doc.exists) {
        this.obj=doc.data();
        this.insertInWishProdModel(this.obj);


        this.array=Object.values(this.obj);



        for(let i=0;i<this.array.length;i++) {
          if(!(this.array[i]===' ')) {
            this.imageService.getFilteredList('id', this.array[i]);
            this.imageService.imageDetailList.snapshotChanges().subscribe(
              list => {
                this.wishedProductList = list.map(item => {
                  return item.payload.val();
                });
                this.getValues(this.wishedProductList, i);
                this.prodService.setTotal(this.total);
              }
            );
          }
        }


      } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
      }
    }).catch((error) => {
      console.log("Error getting document:", error);
    });

  }
  insertInWishProdModel(meh: any){
    this.wishListList=meh as wishedProductModel;
    this.arrFields=Object.keys(this.wishListList);
    this.arrVal=Object.values(this.wishListList);

    console.log("From Interface and function: ",this.wishListList);

  }
  delete(id:string){
    console.log('inside delete');
    let cnt=0;
    for(let  i=0;i<this.arrVal.length;i++){
          if(this.arrVal[i]===id){
            break;
          }
          cnt++;
      console.log('inside for loop delete');
    }
    console.log('out');
    console.log(this.arrFields[cnt]);

    this.db.collection('wished').doc(this.auth.getId()).set({[this.arrFields[cnt]]:' '},{ merge: true })


  }
  getValues(whatever: productModel[],i:number){
    this.total+=Number(whatever[0].price);
    this.copyList[i]=whatever[0];


    this.filled=true;

  }

  counter() {
    return Array.from(Array(Math.ceil(this.copyList.length)).keys())
  }

}
