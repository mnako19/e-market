import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {NavbarComponent} from "./comp/navbar/navbar.component";
import {HomeComponent} from "./comp/home/home.component";
import {LoginComponent} from "./comp/login/login.component";
import {AuthGuardService} from "./auth-guard.service"
import {AdminComponent} from "./admin/admin.component";
import firebase from "firebase";
import {ImageComponent} from "./images/image/image.component";
import {ImagesComponent} from "./images/images.component";
import Auth = firebase.auth.Auth;
import {ImageListComponent} from "./images/image-list/image-list.component";
import {WishlistComponent} from "./wishlist/wishlist.component";
import {ShowComponent} from "./show/show.component";
import {SignupComponent} from "./comp/signup/signup.component";
import {CheckoutComponent} from "./checkout/checkout.component";

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path:'signup',component:SignupComponent},
  {path: 'login', component: LoginComponent},
  {path: 'home', component: HomeComponent ,},
  {path: 'admin', component: AdminComponent},
  {path:'checkout', component:CheckoutComponent},
  {path:'images', component: ImagesComponent, children:[
      {path:'upload', component: ImageComponent, canActivate:[AuthGuardService]},
      {path:'list', component:ImageListComponent, canActivate:[AuthGuardService] }
    ]},
  {path:'wishlist', component:  WishlistComponent},
  {path:'show', component: ShowComponent, canActivate: [AuthGuardService]},
  {path: '**', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
