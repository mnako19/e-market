import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './comp/navbar/navbar.component';
import { HomeComponent } from './comp/home/home.component';
import {ROUTES} from "@angular/router";
import {RouterModule} from "@angular/router";
import {LoginComponent} from "./comp/login/login.component";
import {environment} from "../environments/environment";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {AngularFireModule} from "@angular/fire";
import {AngularFireAuthModule} from "@angular/fire/auth";
import {AuthenticationService} from "./authentication.service";
import {AngularFireStorageModule} from "@angular/fire/storage";

import { AdminComponent } from './admin/admin.component';
import { ImagesComponent } from './images/images.component';
import { ImageComponent } from './images/image/image.component';
import { ImageListComponent } from './images/image-list/image-list.component';

import {ReactiveFormsModule} from '@angular/forms';
import { BgComponent } from './bg/bg.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { ShowComponent } from './show/show.component';
import { SignupComponent } from './comp/signup/signup.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { FootersComponent } from './footers/footers.component';


@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        HomeComponent,
        AdminComponent,
        ImagesComponent,
        ImageComponent,
        ImageListComponent,
        BgComponent,
        LoginComponent,
        WishlistComponent,
        ShowComponent,
        SignupComponent,
        CheckoutComponent,
        FootersComponent,
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    ReactiveFormsModule
  ],
  providers: [AuthenticationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
