import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl} from "@angular/forms";
import {Validators} from "@angular/forms";
import {AngularFireStorage} from "@angular/fire/storage";
import {finalize} from "rxjs/operators";
import {ImageService} from "../../shared/image.service";
import {AuthenticationService} from "../../authentication.service";

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {
  imgSrc: string ='/assets/img/Placeholder.jpg';
  user: string='';

 formTemplate= new FormGroup({
   name: new FormControl('',Validators.required),
   caption: new FormControl('',Validators.required),
   category: new FormControl('',Validators.required),
   quantity: new FormControl('', Validators.required),
   price: new FormControl('',Validators.required),
   imageUrl: new FormControl('', Validators.required),
   id: new FormControl('',Validators.required)
 });


  selectedImage: any=null;
  isSubitted: boolean=false;

  constructor(private storage: AngularFireStorage, private service: ImageService) { }


  ngOnInit(): void {
    this.resetFrom()
  }



  showPreview(event: any){
  if(event.target.files && event.target.files[0]){
    const reader = new FileReader();
    reader.onload = (e:any) => this.imgSrc=e.target.result;
    reader.readAsDataURL(event.target.files[0]);
    this.selectedImage= event.target.files[0];
  }
else {
  this.imgSrc='/assets/img/Placeholder.jpg';
  this.selectedImage=null;
  }

  }

  onSubmit(formValue){
    this.isSubitted=true;
    //checks if the form is validly filled
    if(this.formTemplate.valid){

    var filePath=`${formValue.category}/${this.selectedImage.name}_${new Date().getTime()}`

    const fileRef=this.storage.ref(filePath);

    this.storage.upload(filePath,this.selectedImage).snapshotChanges().pipe(
    finalize(()=>{

    fileRef.getDownloadURL().subscribe((url)=>{

    formValue['imageURL']=url;
    this.service.insertImageDetails(formValue);
    this.resetFrom();
  })
})).subscribe();
}
}


get formControls(){
    return this.formTemplate['controls']
}

resetFrom(){
    this.formTemplate.reset();
    this.formTemplate.setValue({
      name: '',
      id: '',
      caption: '',
      imageUrl: '',
      category:'Makeup',
      price: '',
      quantity: ''

    });
    this.imgSrc='/assets/img/Placeholder.jpg';
    this.isSubitted=false;
    this.selectedImage=null;
}


}
