import { Component, OnInit } from '@angular/core';
import {ImageService} from "../../shared/image.service";
import {productModel} from "../../__model/productModel";
import {ProdService} from "../../shared/prod.service";
import {AngularFirestore} from "@angular/fire/firestore";
import {AuthenticationService} from "../../authentication.service";

@Component({
  selector: 'app-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.css']
})
export class ImageListComponent implements OnInit {
imageList: productModel[];
prodList: productModel[];
rowIndexArray: any[];
  in: number;

  constructor(private service: ImageService, private prodService: ProdService, private firestore: AngularFirestore, private auth: AuthenticationService) { }





  ngOnInit(): void {
   this.service.imageDetailList.snapshotChanges().subscribe(
      list=> {
        this.imageList = list.map(item => {
          return item.payload.val();
        });
        this.rowIndexArray = Array.from(Array(Math.ceil(this.imageList.length / 4)).keys())
      });
    this.in=this.auth.getCounter();
  }

  getProdId(prodId: string) {

    this.firestore.collection('users').doc(this.auth.getDoc()).set({counter:this.in},{ merge: true })
    console.log(this.in);
    this.firestore.collection('wished').doc(this.auth.getId()).set({['prod'+this.in]: prodId},{ merge: true })
    this.in+=1;
  }





}

