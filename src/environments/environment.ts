// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyAw4mFUqrdCaKis_tiSeXYrvgnxaYifKZ8",
    authDomain: "cosmetic-e2c1a.firebaseapp.com",
    databaseURL: "https://cosmetic-e2c1a-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "cosmetic-e2c1a",
    storageBucket: "cosmetic-e2c1a.appspot.com",
    messagingSenderId: "77982139806",
    appId: "1:77982139806:web:07a3d87ba506751cb9028f"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
